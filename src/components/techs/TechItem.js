import React from 'react'
import {connect} from 'react-redux'
import {deleteTech} from '../../actions/techActions'
import M from 'materialize-css/dist/js/materialize.min.js'

const TechItem = ({tech , deleteTech}) => {


    const Delete = ()=>{
        deleteTech(tech.id)

        M.toast({html:'Tech Deleted'})
    }


    return (
        <li className="collection-item">
            <div className="">
                {tech.firstname} {tech.lastname}
                <a onClick={Delete} href="#!" className="secondary-content">
                    <i className="material-icons grey-text">delete</i>
                </a>
            </div>
            
        </li>
    )
}
export  default connect(null , {deleteTech})(TechItem)
