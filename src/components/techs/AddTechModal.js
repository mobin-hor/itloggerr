import React,{useState} from 'react'
import {connect} from 'react-redux'
import {addTech} from '../../actions/techActions'
import M from 'materialize-css/dist/js/materialize.min.js'

const AddTechModal = ({addTech}) => {

    const [firstName , setFirstname] = useState('')
    const [lastName , setLastName] = useState('')

    const onSubmit = ()=>{
        if(firstName === '' || lastName === ''){
            M.toast({html:'Please Enter a firstname and lastname'})
        }else{
            addTech({firstname:firstName ,lastname: lastName})
            M.toast({html:`${firstName} ${lastName} was added as a tech`})
            setFirstname('')
            setLastName('')
        }
    }
    return (
        <div id="add-tech-modal" className="modal">
            <div className="modal-content">
                <h4>New Technicion</h4>
                <div className="row">
                    <div className="input-field">
                        <input type="text" name="firstName" value={firstName} onChange={e=>setFirstname(e.target.value)}/>
                        <label htmlFor="firsName" className="active">
                            firstName
                        </label>

                    </div>
                </div>
                <div className="row">
                    <div className="input-field">
                        <input type="text" name="lastName" value={lastName} onChange={e=>setLastName(e.target.value)}/>
                        <label htmlFor="firsName" className="active">
                            lastName
                        </label>

                    </div>
                </div>

            </div>
            <div className="modal-footer">
                <a href="#!" onClick={onSubmit} className="modal-close waves-effect waves-light blue btn">Enter</a>
            </div>
        </div>
    )
}


export default connect(null , {addTech})(AddTechModal)